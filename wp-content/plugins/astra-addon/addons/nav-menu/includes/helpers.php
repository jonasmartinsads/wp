<?php
/**
 * Helper functions for navigation menu addon.
 *
 * @since x.x.x
 * @package Astra Addon
 */

/**
 * Get Link Pinter CSS
 *
 * @since x.x.x
 */
if ( ! function_exists( 'astra_get_link_pointer_css' ) ) :
	/**
	 * Get Link Pinter CSS
	 *
	 * @since x.x.x
	 *
	 * @param  string $prefix CSS Prefix selector.
	 * @param  string $style  Pointer style.
	 * @param  string $color  Color.
	 * @param  string $width  Width.
	 * @return string         Generated CSS.
	 */
	function astra_get_link_pointer_css( $prefix, $style, $color, $width ) {

		if ( 'none' === $style ) {
			return '';
		}

		$css = array();

		if ( 'underline' === $style ) {
			$css = array(
				'.ast-desktop ' . $prefix . ' .ast-link-pointer-style-underline > li > a:before' => array(
					'background-color' => esc_attr( $color ),
					'height'           => esc_attr( $width ) . 'px',
				),
			);
		} elseif ( 'overline' === $style ) {
			$css = array(
				'.ast-desktop ' . $prefix . ' .ast-link-pointer-style-overline > li > a:before' => array(
					'background-color' => esc_attr( $color ),
					'height'           => esc_attr( $width ) . 'px',
				),
			);
		}

		/* Parse CSS from array() */
		return astra_parse_css( $css, astra_header_break_point(), '' );
	}
endif;
