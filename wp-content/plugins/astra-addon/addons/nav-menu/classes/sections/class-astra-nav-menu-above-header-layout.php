<?php
/**
 * Mega Menu Options configurations.
 *
 * @package     Astra Addon
 * @author      Brainstorm Force
 * @copyright   Copyright (c) 2015, Brainstorm Force
 * @link        http://www.brainstormforce.com
 * @since       x.x.x
 */

// Block direct access to the file.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Bail if Customizer config base class does not exist.
if ( ! class_exists( 'Astra_Customizer_Config_Base' ) ) {
	return;
}

if ( ! class_exists( 'Astra_Nav_Menu_Above_Header_Layout' ) ) {

	/**
	 * Register Mega Menu Customizer Configurations.
	 */
	class Astra_Nav_Menu_Above_Header_Layout extends Astra_Customizer_Config_Base {

		/**
		 * Register Mega Menu Customizer Configurations.
		 *
		 * @param Array                $configurations Astra Customizer Configurations.
		 * @param WP_Customize_Manager $wp_customize instance of WP_Customize_Manager.
		 * @since x.x.x
		 * @return Array Astra Customizer Configurations with updated configurations.
		 */
		public function register_configuration( $configurations, $wp_customize ) {

			$_configs = array(

				// Option: Pointer effect.
				array(
					'name'     => ASTRA_THEME_SETTINGS . '[above-nav-menu-pointer-effect]',
					'default'  => astra_get_option( 'above-nav-menu-pointer-effect' ),
					'type'     => 'control',
					'control'  => 'select',
					'section'  => 'section-above-header',
					'priority' => 95,
					'title'    => __( 'Link Pointer Style', 'astra-addon' ),
					'required' => array(
						'conditions' => array(
							array( ASTRA_THEME_SETTINGS . '[above-header-section-1]', '==', 'menu' ),
							array( ASTRA_THEME_SETTINGS . '[above-header-section-2]', '==', 'menu' ),
						),
						'operator'   => 'OR',
					),
					'choices'  => array(
						'none'      => __( 'None', 'astra-addon' ),
						'underline' => __( 'Underline', 'astra-addon' ),
						'overline'  => __( 'Overline', 'astra-addon' ),
					),
				),

				// Option: Pointer color.
				array(
					'name'      => ASTRA_THEME_SETTINGS . '[above-nav-menu-pointer-color]',
					'default'   => '#0274be',
					'type'      => 'control',
					'transport' => 'postMessage',
					'control'   => 'ast-color',
					'section'   => 'section-above-header',
					'priority'  => 95,
					'title'     => __( 'Link Pointer Color', 'astra-addon' ),
					'required'  => array(
						ASTRA_THEME_SETTINGS . '[above-nav-menu-pointer-effect]',
						'!=',
						'none',
					),
				),

				// Option: Pointer width.
				array(
					'name'        => ASTRA_THEME_SETTINGS . '[above-nav-menu-pointer-thickness]',
					'transport'   => 'postMessage',
					'default'     => astra_get_option( 'above-nav-menu-pointer-thickness' ),
					'type'        => 'control',
					'control'     => 'number',
					'required'    => array(
						ASTRA_THEME_SETTINGS . '[above-nav-menu-pointer-effect]',
						'!=',
						'none',
					),
					'section'     => 'section-above-header',
					'priority'    => 95,
					'title'       => __( 'Link Pointer Thickness', 'astra-addon' ),
					'input_attrs' => array(
						'min'  => 0,
						'step' => 1,
						'max'  => 20,
					),
				),

				// Option - Megamenu Heading Space.
				array(
					'name'           => ASTRA_THEME_SETTINGS . '[above-header-megamenu-heading-space]',
					'default'        => astra_get_option( 'above-header-megamenu-heading-space' ),
					'type'           => 'control',
					'transport'      => 'postMessage',
					'control'        => 'ast-responsive-spacing',
					'priority'       => 175,
					'title'          => __( 'Megamenu Heading Space', 'astra-addon' ),
					'linked_choices' => true,
					'unit_choices'   => array( 'px', 'em', '%' ),
					'choices'        => array(
						'top'    => __( 'Top', 'astra-addon' ),
						'right'  => __( 'Right', 'astra-addon' ),
						'bottom' => __( 'Bottom', 'astra-addon' ),
						'left'   => __( 'Left', 'astra-addon' ),
					),
					'section'        => 'section-above-header',
					'required'       => array(
						'conditions' => array(
							array( ASTRA_THEME_SETTINGS . '[above-header-section-1]', '==', 'menu' ),
							array( ASTRA_THEME_SETTINGS . '[above-header-section-2]', '==', 'menu' ),
						),
						'operator'   => 'OR',
					),
				),
			);

			$configurations = array_merge( $configurations, $_configs );

			return $configurations;
		}
	}
}

new Astra_Nav_Menu_Above_Header_Layout;

