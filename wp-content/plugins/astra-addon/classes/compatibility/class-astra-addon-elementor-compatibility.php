<?php
/**
 * Astra Addon Customizer
 *
 * @package Astra Addon
 * @since x.x.x
 */

if ( ! class_exists( 'Astra_Addon_Elementor_Compatibility' ) ) :

	/**
	 * Astra Addon Page Builder Compatibility base class
	 *
	 * @since x.x.x
	 */
	class Astra_Addon_Elementor_Compatibility extends Astra_Addon_Page_Builder_Compatibility {

		/**
		 * Instance
		 *
		 * @since x.x.x
		 *
		 * @access private
		 * @var object Class object.
		 */
		private static $instance;

		/**
		 * Initiator
		 *
		 * @since x.x.x
		 *
		 * @return object initialized object of class.
		 */
		public static function get_instance() {
			if ( ! isset( self::$instance ) ) {
				self::$instance = new self;
			}
			return self::$instance;
		}

		/**
		 * Render content for post.
		 *
		 * @param int $post_id Post id.
		 *
		 * @since x.x.x
		 */
		public function render_content( $post_id ) {

			// set post to glabal post.
			$elementor_instance = Elementor\Plugin::instance();
			echo $elementor_instance->frontend->get_builder_content_for_display( $post_id );
		}

		/**
		 * Load styles and scripts.
		 *
		 * @param int $post_id Post id.
		 *
		 * @since x.x.x
		 */
		public function enqueue_scripts( $post_id ) {

			if ( class_exists( '\Elementor\Plugin' ) ) {
				$elementor = \Elementor\Plugin::instance();
				$elementor->frontend->enqueue_styles();
			}
			if ( class_exists( '\ElementorPro\Plugin' ) ) {
				$elementor = \ElementorPro\Plugin::instance();
				$elementor->enqueue_styles();
			}
			if ( '' !== $post_id && class_exists( '\Elementor\Post_CSS_File' ) ) {
				$css_file = new \Elementor\Post_CSS_File( $post_id );
				$css_file->enqueue();
			}
		}

	}

endif;
